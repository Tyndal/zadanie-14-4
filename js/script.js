const movies = [
  {
    id: 1,
    pic: "images/hobbit.jpg",
    title: "Hobbit",
    desc:
      "A reluctant Hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home, and the gold within it from the dragon Smaug.",
    prodDate: "2012",
    mins: "584 minutes",
    filmType: "fantasy",
    trailer: "https://youtu.be/OPVWy1tFXuc"
  },
  {
    id: 2,
    pic: "images/lotr.jpg",
    title: "The Lord of the Rings",
    desc:
      "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.",
    prodDate: "2001",
    mins: "684 minutes",
    filmType: "fantasy",
    trailer: "https://youtu.be/V75dMMIW2B4"
  },
  {
    id: 3,
    pic: "images/warcraft.jpg",
    title: "Warcraft",
    desc:
      "The peaceful realm of Azeroth stands on the brink of war as its civilization faces a fearsome race of invaders: Orc warriors fleeing their dying home to colonize another. As a portal opens to connect the two worlds, one army faces destruction and the other faces extinction.",
    prodDate: "2016",
    mins: "123 minutes",
    filmType: "fantasy",
    trailer: "https://youtu.be/2Rxoz13Bthc"
  }
];

// Title
const MovieTitle = React.createClass({
  propTypes: {
    title: React.PropTypes.string.isRequired
  },
  render: function() {
    return React.createElement("h2", {className: 'movie-title'}, this.props.title);
  }
});

//Details
const MovieDetails = React.createClass({
  propTypes: {
    movie: React.PropTypes.object.isRequired
  },
  render: function() {
    return React.createElement(
      "div",
      { className: "date-mins-flex" },
      React.createElement("h4", {}, this.props.movie.prodDate),
      React.createElement("h4", {}, this.props.movie.mins),
      React.createElement("h4", {}, this.props.movie.filmType)
    );
  }
});

//Description
const MovieDesc = React.createClass({
  propTypes: {
    desc: React.PropTypes.string.isRequired
  },
  render: function() {
    return React.createElement("p", {}, this.props.desc);
  }
});

//Button
const MovieButton = React.createClass({
  propTypes: {
    trailer: React.PropTypes.string.isRequired
  },
  render: function() {
    return React.createElement(
      "button",
      { className: "trailer-btn" },
      React.createElement(
        "a",
        { href: this.props.trailer, target: "_blank" },
        "Trailer"
      )
    );
  }
});

//Poster
const MoviePoster = React.createClass({
  propTypes: {
    image: React.PropTypes.string.isRequired
  },
  render: function() {
    return React.createElement(
      "div",
      { className: "imgContainer" },
      React.createElement("img", { src: this.props.image })
    );
  }
});

//Movie info (right side)
const MovieInfo = React.createClass({
  propTypes: {
    movie: React.PropTypes.object.isRequired
  },
  render: function() {
    return React.createElement(
      "div",
      { className: "info-flex" },
      React.createElement(MovieTitle, { title: this.props.movie.title }),
      React.createElement(MovieDetails, { movie: this.props.movie }),
      React.createElement(MovieDesc, { desc: this.props.movie.desc }),
      React.createElement(MovieButton, { trailer: this.props.movie.trailer })
    );
  }
});

const Movie = React.createClass({
  propTypes: {
    movie: React.PropTypes.object.isRequired
  },
  render: function() {
    return React.createElement(
      "div",
      { className: "film-flex" },
      React.createElement(MoviePoster, { image: this.props.movie.pic }, ""),
      React.createElement(MovieInfo, { movie: this.props.movie }, "")
    );
  }
});

//Tworzymy tablice
const moviesElements = movies.map(function(movie) {
  return React.createElement(Movie, { key: movie.id, movie: movie });
});

// Umieszczamy powyższe w liscie
const element = React.createElement(
  "div",
  {},
  React.createElement(
    "div",
    { className: "container" },
    React.createElement("h1", { className: "main-header" }, "Movies list"),
    React.createElement("ul", { className: "movie-list" }, moviesElements)
  )
);

ReactDOM.render(element, document.getElementById("app"));
